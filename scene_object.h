/***********************************************************
     Starter code for Assignment 3

     This code was originally written by Jack Wang for
		    CSC418, SPRING 2005

		classes defining primitives in the scene

***********************************************************/

#include <vector>
#include "util.h"

#define EPSILON 0.000001

// All primitives should provide a intersection function.
// To create more primitives, inherit from SceneObject.
// Namely, you can create, Sphere, Cylinder, etc... classes
// here.
class SceneObject {
public:
	// Returns true if an intersection occured, false otherwise.
	virtual bool intersect( Ray3D&, const Matrix4x4&, const Matrix4x4& ) = 0;
public:
	Point3D position;
};

// Example primitive you can create, this is a unit square on
// the xy-plane.
class UnitSquare : public SceneObject {
public:
	bool intersect( Ray3D& ray, const Matrix4x4& worldToModel,
			const Matrix4x4& modelToWorld );

	virtual void set_local_hit_point(Ray3D& ray, const Matrix4x4& worldToModel){

		Point3D local_hit, local_position;

		local_hit = worldToModel * ray.intersection.point;
		local_position = worldToModel * position;

		ray.intersection.local_point[0] = local_hit[0] - local_position[0];
		ray.intersection.local_point[1] = local_hit[1] - local_position[1];
		ray.intersection.local_point[2] = ray.intersection.point[2];


	};
};

class UnitSphere : public SceneObject {
public:
	UnitSphere(){radius = 1.0;}

	bool intersect( Ray3D& ray, const Matrix4x4& worldToModel,
			const Matrix4x4& modelToWorld );

	virtual void set_local_hit_point(Ray3D& ray){

		ray.intersection.local_point[0] = ray.intersection.point[0] - position[0];
		ray.intersection.local_point[1] = ray.intersection.point[1] - position[1];
		ray.intersection.local_point[2] = (ray.intersection.point[2] - position[2])/radius;

	};

	// virtual void set_local_hit_point(Ray3D& ray, const Matrix4x4& worldToModel){
	//
	// 	Point3D local_hit, local_position;
	//
	// 	local_hit = worldToModel * ray.intersection.point;
	// 	local_position = worldToModel * position;
	//
	// 	ray.intersection.local_point[0] = local_hit[0] - local_position[0];
	// 	ray.intersection.local_point[1] = local_hit[1] - local_position[1];
	// 	ray.intersection.local_point[2] = (local_hit[2] - local_position[2])/radius;
	//
	// };


public:
	float radius; // sphere
};


class UnitCylinder : public SceneObject {
public:
	bool intersect( Ray3D& ray, const Matrix4x4& worldToModel,
			const Matrix4x4& modelToWorld );
};

// For creation of arbitrary object models
// ref: http://www.opengl-tutorial.org/beginners-tutorials/tutorial-7-model-loading/
//
class ObjModel : public SceneObject {
	std::vector< Face > faces;
public:
	ObjModel(const char * path);

	bool loadOBJ( const char * path,
				std::vector <Face> & out_faces);

	bool intersect( Ray3D& ray, const Matrix4x4& worldToModel,
			const Matrix4x4& modelToWorld);

};
