/***********************************************************
     Parsing of TGA file format for Texture Loading
     ref: http://talkera.org/opengl/lesson-8-parsing-and-loading-custom-image-formats/

***********************************************************/
#include "util.h"
#include <cmath>


class Image
{
public:
    Image() {}
    virtual ~Image(){};

    bool load(const char *filename);
    void release();
public:
    unsigned char *imageData;
    int width;
    int length;
private:
    struct tga_header
    {
        unsigned char idLength;
        unsigned char colorMapType;
        unsigned char imageTypeCode;
        unsigned char colorMapSpec[5];
        unsigned short xOrigin;
        unsigned short yOrigin;
        unsigned short width;
        unsigned short height;
        unsigned char bpp;
        unsigned char imageDesc;
    };

    tga_header tgaheader;
};

class Mapping
{
public:
    Mapping() {};
    virtual ~Mapping() {};

    virtual void get_texel_coordinates (Point3D &local_hit_point,
                                        const int hres,
                                        const int vres,
                                        int& row,
                                        int& column) const {};
};

class PlanarMap: public Mapping
{
public:
    PlanarMap(): Mapping() {};
    virtual ~PlanarMap() {};

    virtual void get_texel_coordinates (Point3D &local_hit_point,
                                        const int hres,
                                        const int vres,
                                        int &row,
                                        int &column) const
    {
        // float u = 1 - (local_hit_point[0] - floor(local_hit_point[0]) / 2);
        // float v = 1 - (local_hit_point[1] - floor(local_hit_point[1]) / 2);


        float u = fmod(2 * (local_hit_point[0] - floor(local_hit_point[0])), 1);
        float v = fmod(2 * (local_hit_point[1] - floor(local_hit_point[1])), 1);


        // printf("u: %f || v: %f \n", u, v);

        column = (int)((hres - 1) * u);
        row = (int)((vres - 1) * v);

        // printf("col: %i || row: %i \n", column, row);

    };
};


class SphericalMap: public Mapping
{
public:
    SphericalMap(): Mapping() {};
    virtual ~SphericalMap() {};

    virtual void get_texel_coordinates (Point3D &local_hit_point,
                                        const int hres,
                                        const int vres,
                                        int& row,
                                        int& column) const
    {
        float theta = acos(local_hit_point[2]);
        float phi = atan2(local_hit_point[1], local_hit_point[0]);


        if(phi < 0.0)
            phi += 2*M_PI;

        float u = phi/(2*M_PI);
        float v = (M_PI - theta)/M_PI;
        // float u = atan2(local_hit_point[1], local_hit_point[0])/(2*M_PI);
        // float v = acosf(local_hit_point[2] / (double)10) / M_PI ;``


        column = (int)((hres - 1) * u);
        row = (int)((vres - 1) * v);

    };
};

class Texture : public Material
{
public:

    Texture(char *filename):Material() {
        image_ptr = new Image;
        image_ptr->load(filename);
    }

    virtual ~Texture() {};

    virtual void set_mapping(Mapping *mapping)
    { mapping_ptr = mapping;}

    virtual Colour get_color(Ray3D& ray){
        int row, col;

        if(mapping_ptr){
            mapping_ptr->get_texel_coordinates(ray.intersection.local_point,
                                                    image_ptr->width,
                                                    image_ptr->length,
                                                    row,
                                                    col);
        }

        int index = std::abs(std::abs(row) * 3 * image_ptr->width + 3 * std::abs(col));

        // printf("color: %d:%d:%d\n", image_ptr->imageData[index], image_ptr->imageData[index+1], image_ptr->imageData[index+2]);

        return Colour(image_ptr->imageData[index]/255.0,
                        image_ptr->imageData[index+1]/255.0,
                        image_ptr->imageData[index+2]/255.0);

    };

public:
    Image* image_ptr;
    Mapping* mapping_ptr;
};


// =============================================================================
// Environment Mapping
//
class Environment : public Material
{
public:

    Environment(char *top, char *bottom, char *left, char *right ,char *front ,char *back): Material() {
        top_ptr = new Image;
        bottom_ptr = new Image;
        left_ptr = new Image;
        right_ptr = new Image;
        front_ptr = new Image;
        back_ptr = new Image;

        top_ptr->load(top);
        bottom_ptr->load(bottom);
        left_ptr->load(left);
        right_ptr->load(right);
        front_ptr->load(front);
        back_ptr->load(back);
    }

    virtual ~Environment() {};

    virtual void set_mapping(Mapping *mapping)
    { mapping_ptr = mapping; }

    Colour get_color(Ray3D& ray);

    Colour readTexel(Image *img, float u, float v);

public:
    Image *top_ptr, *bottom_ptr, *left_ptr, *right_ptr, *front_ptr, *back_ptr;
    Mapping* mapping_ptr;
};
