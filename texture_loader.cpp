
#include "texture_loader.h"
#include <fstream>
#include <iostream>
#include <cstdlib>

using namespace std;

bool Image::load(const char* filename)
{
    FILE* f;

    f = fopen(filename, "rb");

    if(f)
    {
        // read TGA header
        fread((char *) &tgaheader, sizeof(unsigned char), sizeof(struct tga_header), f); // read the 54-byte header


        width = tgaheader.width;
        length = tgaheader.height;


        // read pixel data
        int imageSize = tgaheader.width * tgaheader.height * tgaheader.bpp;

        imageData = new unsigned char[imageSize]; // allocate 3 bytes per pixel
        fread(imageData, sizeof(unsigned char), imageSize, f); // read the rest of the data at once


        for(int i = 0; i < imageSize; i += 3)
        {
            unsigned char tmp = imageData[i];
            imageData[i] = imageData[i+2];
            imageData[i+2] = tmp;
        }


        fclose(f);

    } else {
        cout << "Error opening file" << endl;
        return -1;
    }

    return 0;
}

void Image::release()
{
    free(imageData);
}


Colour Environment::get_color(Ray3D& ray){

    Colour outputColor(0.0, 0.0, 0.0);

    // if(!mapping_ptr)
    //     return outputColor;

    // Right or Left
    if ((fabsf(ray.dir[0]) >= fabsf(ray.dir[1]))
                        && (fabsf(ray.dir[0]) >= fabsf(ray.dir[2])))
    {
        // Left
        if(ray.dir[0] > 0.0f){
            outputColor = readTexel(left_ptr,
                    1.0f - (ray.dir[2]/ray.dir[0] + 1.0f) * 0.5f,
                    (ray.dir[1]/ray.dir[0] + 1.0f) * 0.5f);
        }
        // Right
        else if (ray.dir[0] < 0.0f){
            outputColor = readTexel(right_ptr,
                    1.0f - (ray.dir[2]/ray.dir[0] + 1.0f) * 0.5f,
                    1.0f - (ray.dir[1]/ray.dir[0] + 1.0f) * 0.5f);
        }
    }
    // Top or Bottom
    else if ((fabsf(ray.dir[1]) >= fabsf(ray.dir[0]))
                        && (fabsf(ray.dir[1]) >= fabsf(ray.dir[2])))
    {
        // Top
        if(ray.dir[1] > 0.0f){
            outputColor = readTexel(top_ptr,
                    (ray.dir[0]/ray.dir[1] + 1.0f) * 0.5f,
                    1.0f - (ray.dir[2]/ray.dir[1] + 1.0f) * 0.5f);
        }
        // Bottom
        else if (ray.dir[1] < 0.0f){
            outputColor = readTexel(bottom_ptr,
                    1.0f - (ray.dir[0]/ray.dir[1] + 1.0f) * 0.5f,
                    (ray.dir[2]/ray.dir[1] + 1.0f) * 0.5f);
        }
    }

    // Front or Back
    else if ((fabsf(ray.dir[2]) >= fabsf(ray.dir[0]))
                        && (fabsf(ray.dir[2]) >= fabsf(ray.dir[1])))
    {
        // Back
        if(ray.dir[2] > 0.0f){
            outputColor = readTexel(back_ptr,
                    (ray.dir[0]/ray.dir[2] + 1.0f) * 0.5f,
                    (ray.dir[1]/ray.dir[2] + 1.0f) * 0.5f);
        }
        // Front
        else if (ray.dir[2] < 0.0f){
            outputColor = readTexel(front_ptr,
                    (ray.dir[0]/ray.dir[2] + 1.0f) * 0.5f,
                    1.0f - (ray.dir[1]/ray.dir[2] + 1.0f) * 0.5f);
        }
    }

    return outputColor;
}


Colour Environment::readTexel(Image *img, float u, float v){

    int sizeU = img->width;
    int sizeV = img->length;

    u = fabsf(u);
    v = fabsf(v);

    int row, column;
    float r, g, b;


    column = (int)((sizeU - 1) * u);
    row = (int)((sizeV - 1) * v);

    int index = abs(abs(row) * 3 * img->width + 3 * abs(column));

    return Colour(img->imageData[index]/255.0,
                        img->imageData[index+1]/255.0,
                        img->imageData[index+2]/255.0);

}
