/***********************************************************
     Starter code for Assignment 3

     This code was originally written by Jack Wang for
		    CSC418, SPRING 2005

		Implementations of functions in raytracer.h,
		and the main function which specifies the
		scene to be rendered.

***********************************************************/


#include "raytracer.h"
#include "bmp_io.h"
#include <cmath>
#include <iostream>
#include <cstdlib>
#include <string.h>

Raytracer::Raytracer() : _lightSource(NULL), _env(NULL) {
	_root = new SceneDagNode();
}

Raytracer::~Raytracer() {
	delete _root;
}

SceneDagNode* Raytracer::addObject( SceneDagNode* parent,
		SceneObject* obj, Material* mat ) {
	SceneDagNode* node = new SceneDagNode( obj, mat );
	node->parent = parent;
	node->next = NULL;
	node->child = NULL;

	node->hasTexture = false;

	// Add the object to the parent's child list, this means
	// whatever transformation applied to the parent will also
	// be applied to the child.
	if (parent->child == NULL) {
		parent->child = node;
	}
	else {
		parent = parent->child;
		while (parent->next != NULL) {
			parent = parent->next;
		}
		parent->next = node;
	}

	// check if square
	UnitSquare *square = dynamic_cast<UnitSquare *>(node->obj);
	if(square){
		// assume uniform scale factor
		((UnitSphere *)node->obj)->position = Point3D(-1, -1, 0);
	}

	return node;
}

void Raytracer::setTexture(SceneDagNode* node, Texture* txture){
	node->texture = txture;
	node->hasTexture = true;
}

void Raytracer::setEnvironment(Environment *env){
	_env = env;

}

LightListNode* Raytracer::addLightSource( LightSource* light ) {
	LightListNode* tmp = _lightSource;
	_lightSource = new LightListNode( light, tmp );
	return _lightSource;
}

void Raytracer::rotate( SceneDagNode* node, char axis, double angle ) {
	Matrix4x4 rotation;
	double toRadian = 2*M_PI/360.0;
	int i;

	for (i = 0; i < 2; i++) {
		switch(axis) {
			case 'x':
				rotation[0][0] = 1;
				rotation[1][1] = cos(angle*toRadian);
				rotation[1][2] = -sin(angle*toRadian);
				rotation[2][1] = sin(angle*toRadian);
				rotation[2][2] = cos(angle*toRadian);
				rotation[3][3] = 1;
			break;
			case 'y':
				rotation[0][0] = cos(angle*toRadian);
				rotation[0][2] = sin(angle*toRadian);
				rotation[1][1] = 1;
				rotation[2][0] = -sin(angle*toRadian);
				rotation[2][2] = cos(angle*toRadian);
				rotation[3][3] = 1;
			break;
			case 'z':
				rotation[0][0] = cos(angle*toRadian);
				rotation[0][1] = -sin(angle*toRadian);
				rotation[1][0] = sin(angle*toRadian);
				rotation[1][1] = cos(angle*toRadian);
				rotation[2][2] = 1;
				rotation[3][3] = 1;
			break;
		}
		if (i == 0) {
		    node->trans = node->trans*rotation;
			angle = -angle;
		}
		else {
			node->invtrans = rotation*node->invtrans;
		}
	}
}

void Raytracer::translate( SceneDagNode* node, Vector3D trans ) {
	Matrix4x4 translation;

	translation[0][3] = trans[0];
	translation[1][3] = trans[1];
	translation[2][3] = trans[2];
	node->trans = node->trans*translation;
	translation[0][3] = -trans[0];
	translation[1][3] = -trans[1];
	translation[2][3] = -trans[2];
	node->invtrans = translation*node->invtrans;

	// keep track of position for Texture
	node->obj->position[0] = trans[0];
	node->obj->position[1] = trans[1];
	node->obj->position[2] = trans[2];

}

void Raytracer::scale( SceneDagNode* node, Point3D origin, double factor[3] ) {
	Matrix4x4 scale;

	scale[0][0] = factor[0];
	scale[0][3] = origin[0] - factor[0] * origin[0];
	scale[1][1] = factor[1];
	scale[1][3] = origin[1] - factor[1] * origin[1];
	scale[2][2] = factor[2];
	scale[2][3] = origin[2] - factor[2] * origin[2];
	node->trans = node->trans*scale;
	scale[0][0] = 1/factor[0];
	scale[0][3] = origin[0] - 1/factor[0] * origin[0];
	scale[1][1] = 1/factor[1];
	scale[1][3] = origin[1] - 1/factor[1] * origin[1];
	scale[2][2] = 1/factor[2];
	scale[2][3] = origin[2] - 1/factor[2] * origin[2];
	node->invtrans = scale*node->invtrans;

	// sphere? (ghetto type checking)
	UnitSphere *sphere = dynamic_cast<UnitSphere *>(node->obj);
	if(sphere){
		// assume uniform scale factor
		((UnitSphere *)node->obj)->radius = factor[0];
	}


}

Matrix4x4 Raytracer::initInvViewMatrix( Point3D eye, Vector3D view,
		Vector3D up ) {
	Matrix4x4 mat;
	Vector3D w;
	view.normalize();
	up = up - up.dot(view)*view;
	up.normalize();
	w = view.cross(up);

	mat[0][0] = w[0];
	mat[1][0] = w[1];
	mat[2][0] = w[2];
	mat[0][1] = up[0];
	mat[1][1] = up[1];
	mat[2][1] = up[2];
	mat[0][2] = -view[0];
	mat[1][2] = -view[1];
	mat[2][2] = -view[2];
	mat[0][3] = eye[0];
	mat[1][3] = eye[1];
	mat[2][3] = eye[2];

	return mat;
}

void Raytracer::traverseScene( SceneDagNode* node, Ray3D& ray ) {
	SceneDagNode *childPtr;

	// Applies transformation of the current node to the global
	// transformation matrices.
	_modelToWorld = _modelToWorld*node->trans;
	_worldToModel = node->invtrans*_worldToModel;
	if (node->obj) {

		// Perform intersection.
		if (node->obj->intersect(ray, _worldToModel, _modelToWorld)) {

			/**************** Texture Mapping ********************/
			if(node->hasTexture){

				Colour col = node->texture->get_color(ray);

				// Set material to color in texture mapping at local intersection.
				ray.intersection.mat = node->mat;
				ray.intersection.mat->ambient = col;
				ray.intersection.mat->diffuse = col;
				ray.intersection.mat->specular = col;



			} else {
				ray.intersection.mat = node->mat;
			}

		}
	}
	// Traverse the children.
	childPtr = node->child;
	while (childPtr != NULL) {
		traverseScene(childPtr, ray);
		childPtr = childPtr->next;
	}

	// Removes transformation of the current node from the global
	// transformation matrices.
	_worldToModel = node->trans*_worldToModel;
	_modelToWorld = _modelToWorld*node->invtrans;
}

void Raytracer::computeShading( Ray3D& ray ) {
	LightListNode* curLight = _lightSource;
	for (;;) {
		if (curLight == NULL) break;

		// Implement shadows here if needed.
		if(! ray.intersection.none){
			Ray3D shadowRay;
			/**************** compute shadows ********************/
			shadowRay.dir = curLight->light->get_position() - ray.intersection.point;
			shadowRay.dir.normalize();
			shadowRay.origin = ray.intersection.point +0.01*shadowRay.dir;

			//we need to find if the shadow intersects
			traverseScene(_root, shadowRay);
			curLight->light->shade(ray);
			if (! shadowRay.intersection.none ){
				ray.col = Colour(0.0,0.0,0.0);
			}

		}
		// Each lightSource provides its own shading function.
		//curLight->light->shade(ray);
		curLight = curLight->next;
	}
}

void Raytracer::initPixelBuffer() {
	int numbytes = _scrWidth * _scrHeight * sizeof(unsigned char);
	_rbuffer = new unsigned char[numbytes];
	_gbuffer = new unsigned char[numbytes];
	_bbuffer = new unsigned char[numbytes];
	for (int i = 0; i < _scrHeight; i++) {
		for (int j = 0; j < _scrWidth; j++) {
			_rbuffer[i*_scrWidth+j] = 0;
			_gbuffer[i*_scrWidth+j] = 0;
			_bbuffer[i*_scrWidth+j] = 0;
		}
	}
}

void Raytracer::flushPixelBuffer( char *file_name ) {
	bmp_write( file_name, _scrWidth, _scrHeight, _rbuffer, _gbuffer, _bbuffer );
	delete _rbuffer;
	delete _gbuffer;
	delete _bbuffer;
}

Colour Raytracer::shadeRay( Ray3D& ray, int refl_refra_counter ) {

	Colour col(0.0, 0.0, 0.0);
	traverseScene(_root, ray);

	// Don't bother shading if the ray didn't hit
	// anything.
	if (!ray.intersection.none) {
		computeShading(ray);
		col = ray.col;
		// You'll want to call shadeRay recursively (with a different ray,
		// of course) here to implement reflection/refraction effects.
		Ray3D reflection_ray, refraction_ray;
		float index_ref_factor = 1.5;
		ray.dir.normalize();
		ray.intersection.normal.normalize();


		/*********************** REFLECTION *******************/
		if(ray.intersection.mat->reflection_coef>0.0){
			//r = d – 2(d .n)n
			double glossy_reflection_counter = 1.0;
			if (ray.intersection.mat->glossy){
				glossy_reflection_counter = 5.0;
			}
			double colour_array[3];
			colour_array[0] =0;
			colour_array[1] =0;
			colour_array[2] =0;

			float reflection_factor = ray.intersection.mat->reflection_coef;
			for (float i =0; i<glossy_reflection_counter; i++){

				reflection_ray.dir = ray.dir - 2*(ray.dir.dot(ray.intersection.normal)*ray.intersection.normal);
				reflection_ray.dir[0] += i*((double) rand() / (RAND_MAX))*reflection_ray.dir[0];
				reflection_ray.dir[1] += i*((double) rand() / (RAND_MAX))*reflection_ray.dir[1];
				reflection_ray.dir[2] += i*((double) rand() / (RAND_MAX))*reflection_ray.dir[2];
				reflection_ray.dir.normalize();
				reflection_ray.origin = ray.intersection.point+0.01*reflection_ray.dir ;
				if(refl_refra_counter<3){
					Colour reflection_col = shadeRay(reflection_ray, refl_refra_counter+1);
		        	//setup the reflection color
		        	//col = (1-ray.intersection.mat->reflection_coef)*col +(ray.intersection.mat->reflection_coef)*reflection_col;
		        	colour_array[0]=colour_array[0] +(reflection_factor*reflection_col[0])/glossy_reflection_counter;
					colour_array[1]=colour_array[1] +(reflection_factor*reflection_col[1])/glossy_reflection_counter;
					colour_array[2]=colour_array[2] +(reflection_factor * reflection_col[2])/glossy_reflection_counter;

				}


			}
			// float reflection_factor = ray.intersection.mat->reflection_coef;
			col[0] = (1- reflection_factor)* col[0] + colour_array[0];
			col[1] = (1- reflection_factor)* col[1] + colour_array[1];
			col[2] = (1- reflection_factor)* col[2] + colour_array[2];


		}



		/******************* REFRACTION *********************/
	    if (ray.intersection.mat->opacity > 0.0 && refl_refra_counter<3){

			double cosIn = ray.dir.dot(ray.intersection.normal);
			double sinIn = sqrt(1-pow(cosIn,2));
			double n1 =1.3;
			double n2 =1;
			int in_obj =-1;
			//determine which direction is the refraction
			if (cosIn < 0){
				in_obj = 1;
				n1= 1;
				n2 = 1.3;
			}
			double index_ref_factor = n1/n2;
			double cosT2 = 1.0 - index_ref_factor * index_ref_factor * (1.0 - cosIn * cosIn);
			Ray3D refractedRay;
			refractedRay.dir = (index_ref_factor * ray.dir) + (index_ref_factor * cosIn - sqrt(cosT2)) * (in_obj* ray.intersection.normal);
			refractedRay.origin = ray.intersection.point+0.01*refractedRay.dir;
			col = (1- ray.intersection.mat->opacity)* col +  ray.intersection.mat->opacity*shadeRay(refractedRay, refl_refra_counter + 1);

		}
	} else {
		// Environment mapping
		if(_env)
			col = _env->get_color(ray);
	}



	col.clamp();
	return col;





}

void Raytracer::render( int width, int height, Point3D eye, Vector3D view,
		Vector3D up, double fov, char* fileName ) {
	Matrix4x4 viewToWorld;
	_scrWidth = width;
	_scrHeight = height;
	double factor = (double(height)/2)/tan(fov*M_PI/360.0);
	double colour_array[3];
	initPixelBuffer();
	viewToWorld = initInvViewMatrix(eye, view, up);

	// Construct a ray for each pixel.
	for (int i = 0; i < _scrHeight; i++) {
		for (int j = 0; j < _scrWidth; j++) {
			colour_array[0] = 0;
			colour_array[1] = 0;
			colour_array[2] = 0;
			/******************** ANTI-ALIASING *********************/
			for (float k=i; k<i+1.0; k+=0.50f){
				for(float l=j; l<j+1.0; l+=0.50f){

						// Sets up ray origin and direction in view space,
						// image plane is at z = -1.
						Point3D origin(0, 0, 0);
						Point3D imagePlane;
						imagePlane[0] = (-double(width)/2 + 0.5 + l)/factor;
						imagePlane[1] = (-double(height)/2 + 0.5 + k)/factor;
						imagePlane[2] = -1;

						// TODO: Convert ray to world space and call
						// shadeRay(ray) to generate pixel colour.


						Point3D point_in_world = (viewToWorld * imagePlane);
						Vector3D dir = point_in_world - eye;
						Ray3D ray = Ray3D(eye,dir);
						ray.dir.normalize();

						Colour col = shadeRay(ray, 0);

						colour_array[0]+=col[0];
						colour_array[1]+=col[1];
						colour_array[2]+=col[2];


					}


				}
			// double rays_portion = 0.0625;
			double rays_portion = 0.25;
			//0.25;
			// _rbuffer[i*width+j] = int(col[0]*255);
			// _gbuffer[i*width+j] = int(col[1]*255);
			// _bbuffer[i*width+j] = int(col[2]*255);

			//for anti-aliasing
			_rbuffer[i*width+j] = int(colour_array[0]*rays_portion*255);
			_gbuffer[i*width+j] = int(colour_array[1]*rays_portion*255);
			_bbuffer[i*width+j] = int(colour_array[2]*rays_portion*255);
			//resent colour array



		}
	}

	flushPixelBuffer(fileName);
}

int main(int argc, char* argv[])
{
	// Build your scene and setup your camera here, by calling
	// functions from Raytracer.  The code here sets up an example
	// scene and renders it from two different view points, DO NOT
	// change this if you're just implementing part one of the
	// assignment.
	Raytracer raytracer;
	int width = 320;
	int height = 240;
	char *filepath;		// obj filepath

	int flag = 0;

	if (argc == 2) {
		if(strcmp("-s", argv[1]) == 0){
			flag = F_SCENE;
		}

		if(strcmp("-t", argv[1]) == 0){
			flag = F_TXT;
		}
	}

	if (argc == 3) {
		if(strcmp("-o", argv[1]) == 0) {
			filepath = argv[2];
			flag = F_OBJ;
		} else {
			width = atoi(argv[1]);
			height = atoi(argv[2]);
		}
	}

	if (argc == 5) {
		if(strcmp("-o", argv[3]) == 0) {
			filepath = argv[4];
			flag = F_OBJ;
		} else {
			printf("invalid arguments");
			return -1;
		}
	}



	// Camera parameters.

	// Point3D eye(0, 0, 1);
	// Vector3D view(0, 0, -1);

	// Point3D eye(6, 4, -5);
	// Vector3D view(-1,-0.4,-0.1);

	Point3D eye(0, 0, 1);
	Vector3D view(0, 0, -1);

	Vector3D up(0, 1, 0);
	double fov = 60;

	// Defines a material for shading.
	Material gold( Colour(0.3f, 0.3f, 0.3f), Colour(0.75164f, 0.60648f, 0.22648f),
			Colour(0.628281f, 0.555802f, 0.366065f),
			51.2,
			0.0,
			0.0,
			false);

	Material jade( Colour(0, 0, 0), Colour(0.54, 0.89, 0.63),
			Colour(0.316228, 0.316228, 0.316228),
			12.8,
			0.0,
			0.0,
			false );


	Material red(Colour(0.0f,0.0f,0.0f),
					Colour(0.5f,0.0f,0.0f),
					Colour(0.7f,0.6f,0.6f),
					1.0,
					0.0,
					0.0,
					false);
	Material mirror( Colour(0.05, 0.05, 0.05), Colour(0.05, 0.05, 0.05),
                        Colour(0, 0, 0),
                        120,
                         0.0,
                         1.0,
                         false);
	Material white( Colour(0.95f, 0.95f, 0.95f), Colour(0.95f, 0.95f, 0.95f),
                        Colour(0.95f, 0.95f, 0.95f),
                        120.0,
                         0.0,
                         0.6,
                         true);
	Material glass( Colour(0.95f, 0.95f, 0.95f), Colour(0.95f, 0.95f, 0.95f),
                    Colour(0.95f, 0.95f, 0.95f),
                    120,
                     1.0,
                     0.0,
                     false);

	// Defines a point light source.
	// raytracer.addLightSource( new PointLight(Point3D(-5, 10, 5),
	// 			Colour(0.9, 0.9, 0.9) ) );

	// raytracer.addLightSource( new PointLight(Point3D(0, 1, 1),
	// 								Colour(0.9, 0.9, 0.9) ) );



	// Apply some transformations to the unit square.
	double factor1[3] = { 1.0, 2.0, 1.0 };
	double factor2[3] = { -2.0, -2.0, -2.0 };
	double factor3[3] = {1.0,1.0,1.0};

	double factor4[4] = {4.0,4.0,4.0};

	SceneDagNode* objModel;

	// Add a unit square into the scene with material mat.
	if(flag == 0){
		raytracer.addLightSource( new PointLight(Point3D(0, 5, -5),
			Colour(0.9, 0.9, 0.9) ) );

		// Add a unit square into the scene with material mat.
		SceneDagNode* sphere = raytracer.addObject( new UnitSphere(), &white );
		SceneDagNode* plane = raytracer.addObject( new UnitSquare(), &jade );
		SceneDagNode* plane_1 = raytracer.addObject( new UnitSquare(), &jade );
		SceneDagNode* plane_2 = raytracer.addObject( new UnitSquare(), &jade );
		//SceneDagNode* plane_3 = raytracer.addObject( new UnitSquare(), &glass );
		//SceneDagNode* plane_4 = raytracer.addObject( new UnitSquare(), &jade );
		SceneDagNode* sphere2 = raytracer.addObject(new UnitSphere(), &mirror);

		// Apply some transformations to the unit square.
		double factor1[3] = { 1.0, 2.0, 1.0 };
		double factor2[3] = { 6.0, 6.0, 6.0 };
		double factor3[3] = {1.0,1.0,1.0};
		double factor4[3] = {3.0,3.0,3.0};
		raytracer.translate(sphere, Vector3D(-1, 0, -5));
		//raytracer.rotate(sphere, 'x', -45);
		//raytracer.rotate(sphere, 'z', 45);
		raytracer.scale(sphere, Point3D(0, 0, 0), factor3);

		raytracer.translate(plane, Vector3D(0, 0, -7));
		//raytracer.rotate(plane, 'z', 45);
		raytracer.scale(plane, Point3D(0, 0, 0), factor2);

		//

		raytracer.translate(plane_1, Vector3D(0, -3, -4));
		raytracer.rotate(plane_1, 'x', -90);
		raytracer.scale(plane_1, Point3D(0, 0, 0), factor2);
		//raytracer.translate(plane_1, Vector3D(0,-3,3));

		raytracer.translate(plane_2, Vector3D(3, 0, -4));
		raytracer.rotate(plane_2, 'y', -90);
		raytracer.scale(plane_2, Point3D(0, 0, 0), factor2);

		raytracer.translate(sphere2, Vector3D(1.5,-1.0,-5));
		raytracer.rotate(sphere2,'x',-90);
		raytracer.scale(sphere2, Point3D(0,0,0), factor3);

		// Render the scene, feel free to make the image smaller for
		// testing purposes.
		raytracer.render(width, height, eye, view, up, fov, "view1.bmp");

		// Render it from a different point of view.
		//Point3D eye2(4, 2, 1);
		Point3D eye2(-6,4,-5);
		Vector3D view2(1,-0.4,-0.1);
		//Vector3D view2(-4, -2, -6);
		raytracer.render(width, height, eye2, view2, up, fov, "view2.bmp");
	}

	// If obj file specified.
	else if (flag == F_OBJ){

		double factor[4] = {1.0,1.0,1.0};
		raytracer.addLightSource( new PointLight(Point3D(0, 5, -5),
			Colour(0.9, 0.9, 0.9) ) );
		objModel = raytracer.addObject(new ObjModel(filepath), &red);
		// raytracer.translate(objModel, Vector3D(0, 0, -120));
		raytracer.translate(objModel, Vector3D(0, -5, -10));
		raytracer.rotate(objModel, 'y', 45);

		raytracer.scale(objModel, Point3D(0, 0, 0), factor);

		// SceneDagNode* plane = raytracer.addObject( new UnitSquare(), &jade );
		// raytracer.translate(plane, Vector3D(0, 0, -5));
		// raytracer.scale(plane, Point3D(0, 0, 0), factor2);
		//
		// Render the scene, feel free to make the image smaller for
		// testing purposes.
		raytracer.render(width, height, eye, view, up, fov, "lamp1.bmp");

		// Render it from a different point of view.
		//Point3D eye2(4, 2, 1);
		// Point3D eye2(3,0,1);
		// Vector3D view2(1,-0.4,-1);
		// //Vector3D view2(-4, -2, -6);
		// raytracer.rotate(objModel, 'y', 45);
		// raytracer.render(width, height, eye, view, up, fov, "legoman2.bmp");

	} else if (flag == F_SCENE){

		Point3D eye(0, 0, 4);
		Vector3D view(0, 0, -1);
		// Point3D eye(0, 4, 5);
		// Vector3D view(0, -1, -1);

		// ====================
		// Environment mappping
		Environment *env= new Environment("textures/env/skyrender_top.tga",
											"textures/env/skyrender_bottom.tga",
											"textures/env/skyrender_left.tga",
											"textures/env/skyrender_right.tga",
											"textures/env/skyrender_front.tga",
											"textures/env/skyrender_back.tga");
		raytracer.setEnvironment(env);

		double factor[3] = {10.0,10.0,10.0};
		double factor2[3] = {20.0,20.0,20.0};
		double factor3[3] = {3.0,3.0,3.0};

		SceneDagNode* sphere = raytracer.addObject( new UnitSphere(), &mirror );
		// SceneDagNode* floor  = raytracer.addObject( new UnitSquare(), &white );
		// SceneDagNode* l_wall = raytracer.addObject( new UnitSquare(), &gold );
		// SceneDagNode* r_wall = raytracer.addObject( new UnitSquare(), &gold );
		// SceneDagNode* b_wall = raytracer.addObject( new UnitSquare(), &gold );


		raytracer.translate(sphere, Vector3D(0, 0, -4));
		raytracer.scale(sphere, Point3D(0, 0, 0), factor3);


		// raytracer.translate(floor, Vector3D(0, -2, -2));
		// raytracer.rotate(floor, 'x', -90);
		// raytracer.scale(floor, Point3D(0, 0, 0), factor);

		// raytracer.translate(l_wall, Vector3D(-5, 0, -2));
		// raytracer.rotate(l_wall, 'y', 90);
		// raytracer.scale(l_wall, Point3D(0, 0, 0), factor);


		// raytracer.translate(r_wall, Vector3D(5, 0, -2));
		// raytracer.rotate(r_wall, 'y', -90);
		// raytracer.scale(r_wall, Point3D(0, 0, 0), factor);

		// raytracer.translate(b_wall, Vector3D(0, 0, -5));
		// raytracer.scale(b_wall, Point3D(0, 0, 0), factor);

		raytracer.render(1080, 700, eye, view, up, fov, "scene_test.bmp");

	}

	// Texture mapping
	else if (flag == F_TXT){

		raytracer.addLightSource( new PointLight(Point3D(0, 10, -4),
										Colour(0.9, 0.9, 0.9) ) );

		Point3D eye(0, 10, -4);
		Vector3D view(0, -1, -1);


		// create texture and then add object
		Texture *t1 = new Texture("textures/earth.tga");
		// Texture *t2 = new Texture("textures/texture_grass.tga");
		Texture *t2 = new Texture("textures/Cranberries_tile_diffuse.tga");
		// Texture *t2 = new Texture("textures/bricks_red.tga");
		SphericalMap *sm = new SphericalMap();
		PlanarMap *pm = new PlanarMap();

		t1->set_mapping(sm);
		t2->set_mapping(pm);

		// size factors
		double factor[3] = {3.0,3.0,3.0};

		double factor2[3] = {10.0,10.0,10.0};

		Material blank1( Colour(0.95, 0.95, 0.95), Colour(0.95, 0.95, 0.95),
							Colour(0.95, 0.95, 0.95),
							120,
							0.0,
							0.0,
							false);
		Material blank2( Colour(0.95, 0.95, 0.95), Colour(0.95, 0.95, 0.95),
							Colour(0.95, 0.95, 0.95),
							120,
							0.0,
							0.0,
							false);


		// Scene Objects
		SceneDagNode* sphere = raytracer.addObject( new UnitSphere(), &blank1 );
		SceneDagNode* floor  = raytracer.addObject( new UnitSquare(), &blank2 );


		raytracer.setTexture(sphere, t1);
		raytracer.setTexture(floor, t2);

		raytracer.translate(sphere, Vector3D(0, 3, -10));
		// raytracer.translate(sphere, Vector3D(0, -20, 0));
		raytracer.scale(sphere, Point3D(0, 0, 0), factor);

		raytracer.translate(floor, Vector3D(0, -2, -10));
		raytracer.rotate(floor, 'x', -120);
		raytracer.scale(floor, Point3D(0, 0, 0), factor2);



		raytracer.render(width, height, eye, view, up, fov, "texture_map.bmp");
	}



	return 0;
}
