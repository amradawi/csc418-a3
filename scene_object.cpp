/***********************************************************
     Starter code for Assignment 3

     This code was originally written by Jack Wang for
		    CSC418, SPRING 2005

		implements scene_object.h

***********************************************************/

#include <cmath>
#include <iostream>
#include <cstdio>
#include "scene_object.h"
#include <string.h>
#include <stdlib.h>

bool UnitSquare::intersect( Ray3D& ray, const Matrix4x4& worldToModel,
		const Matrix4x4& modelToWorld ) {
	// TODO: implement intersection code for UnitSquare, which is
	// defined on the xy-plane, with vertices (0.5, 0.5, 0),
	// (-0.5, 0.5, 0), (-0.5, -0.5, 0), (0.5, -0.5, 0), and normal
	// (0, 0, 1).
	//
	// Your goal here is to fill ray.intersection with correct values
	// should an intersection occur.  This includes intersection.point,
	// intersection.normal, intersection.none, intersection.t_value.
	//
	// HINT: Remember to first transform the ray into object space
	// to simplify the intersection test.






	// transform ray to object coordinates
	Ray3D ray_object = Ray3D(worldToModel *ray.origin, worldToModel * ray.dir);
	Vector3D normal = Vector3D(0.0, 0.0, 1.0);	//normal vector to plane
	Point3D p1 = Point3D(0.5,0.5,0);	//point on the plane

	/*************************************
	Suppose ray(lamda) = a + lamda* dir
	then
		if the ray intersect there is a lmada such that
		the vector p1 - ray(lamda) on the plane
		i.e (p1-ray(lamda) . Normal) = 0
		so (p1- a) . Normal =  (lamda* dir) . Normal
		so lamda = -(p1 - a)/ dir

	//Vector3D p1_minus_a = p1 - ray_object.origin;
	//project this into the square into x-z plane then we need to find the i
	//intersection of z=0 and z=a[2]+lamda*dir[2] which is when
	// a[2]+ lamda*dir[2] = 0so lamda = -a[2]/dir[2]




	**************************************/


	//computing the necessary part for the intersection condition
	//Vector3D p1_minus_a = p1 - ray_object.origin;
	//project this into the square into x-z plane then we need to find the i
	//intersection of z=0 and z=a[2]+lamda*dir[2] which is when
	// a[2]+ lamda*dir[2] = 0so lamda = -a[2]/dir[2]
	float lamda = float(-ray_object.origin[2])/ray_object.dir[2];
	if(lamda<0){
		return false;
	}
	//float lamda = float (- p1_minus_a.dot(normal))/ ray_object.dir.dot(normal);
	//Vector3D intersection_condition = p1_minus_a - ( lamda * ray_object.dir);
	Point3D point_of_intersection = (ray_object.origin + lamda * ray_object.dir);

	//check if the intersection point is whithin bounds and that the ray has
	// not intersected any objects before or the point of intersection is less
	// than the point that was recorded before.
	if( (0.5 >=point_of_intersection[0])&& (point_of_intersection[0]>=-0.5)
		&& (0.5 >=point_of_intersection[1] )&& (point_of_intersection[1]>=-0.5)
		&& (ray.intersection.none || (lamda < ray.intersection.t_value))){

		//point of intersection in the world coordinates
		ray.intersection.point = modelToWorld * point_of_intersection;

		set_local_hit_point(ray, worldToModel);

		normal.normalize();
		ray.intersection.normal = worldToModel.transpose() * normal;
		ray.intersection.normal.normalize();
		ray.intersection.t_value = lamda;
		ray.intersection.none = false;
		//ray.col = Colour(0.54, 0.89, 0.63);
		return true;


	}

	return false;
}

bool UnitSphere::intersect( Ray3D& ray, const Matrix4x4& worldToModel,
		const Matrix4x4& modelToWorld ) {
	// TODO: implement intersection code for UnitSphere, which is centred
	// on the origin.
	//
	// Your goal here is to fill ray.intersection with correct values
	// should an intersection occur.  This includes intersection.point,
	// intersection.normal, intersection.none, intersection.t_value.
	//
	// HINT: Remember to first transform the ray into object space
	// to simplify the intersection test.

	// transform ray to object coordinates
	Ray3D ray_object = Ray3D(worldToModel *ray.origin, worldToModel * ray.dir);

	/******************************************
	suppose ray(lamda) = a+ lamda*dir;
			sphere equation = (p)^2 -1 = 0
	then
		intersection between ray and sphere is when (a + lamda)^2 - 1 =0
			a^2 + 2a*dir* lamda + dir^2 * lamda^2 -1 = 0
			(dir^2) * lamda^2 + (2a*dir)* lamda + (a^2 - 1) = 0;
			A* lamda^2 + B* lamda + C = 0;
			A = dir*dir;
			B =  2*a* dir;
			C = a*a -1;
			so lamda = (- (B) + - sqrt((B)^2 - 4* (A) * (C))) / 2(A)

	********************************************/


	Vector3D a = ray_object.origin - Point3D(0,0,0);
	double C = a.dot(a) -1;
	double B = 2* a.dot(ray_object.dir);
	double A = ray_object.dir.dot(ray_object.dir);
	double D = pow(B,2) - 4*A*C;

	float lamda_1 = float(-B + sqrt(D))/(2*A);
	float lamda_2 = float(-B - sqrt(D))/(2*A);
	float lamda;

	//if D < 0 there is no intersection points
	if (D < 0){
		return false;
	}else if (D==0){
		lamda = lamda_1;
	}
	else{
		//if both lamda_1 and lamda_2 < 0 the both not visible
		if (lamda_1 < 0 && lamda_2 <0){
			return false;
		//if lamda_1>0 and lamda_2 > 0 then point of intersection is ray(lamda_1)
		} else if (lamda_1 > 0 && lamda_2 <0){
			lamda = lamda_1;
		//point of intersection is ray(lamda_2) since it's the closest
		} else if (lamda_1 > lamda_2 && lamda_2 >0){
			lamda = lamda_2;
		}

	}


	if  (ray.intersection.none || (lamda < ray.intersection.t_value)){

		//calculate point of intersection
		Point3D point_of_intersection = ray_object.origin + lamda * ray_object.dir;
		ray.intersection.point = modelToWorld * point_of_intersection;

		// set_local_hit_point(ray, worldToModel);
		set_local_hit_point(ray);

		//calculate the normal vector
		Vector3D normal_vector = Vector3D(2* point_of_intersection[0],
										2* point_of_intersection[1],
										2* point_of_intersection[2]);
		normal_vector.normalize();
		ray.intersection.normal = worldToModel.transpose() * normal_vector;
		ray.intersection.normal.normalize();
		ray.intersection.none = false;
		ray.intersection.t_value = lamda;
		//ray.col = Colour(0.3, 0.3, 0.3);
		return true	;

	}

	return false;
}



bool UnitCylinder::intersect( Ray3D& ray, const Matrix4x4& worldToModel,
			const Matrix4x4& modelToWorld ){


	 /************************************************
	 A unit right cylinder is of radius 1, and it's origin is (0,0,0)
	 and so the described cyliner has three surfaces : the top, the bottom,
	 and the middle.
	 The top and bottom are circles of radius 1 and thus their equation is

	 (x^2 + y^2 =-0.5) for the bottom with normal (0,0,-1)
	 (x^2 + y^2 = 0.5 ) for the top with normal (0,0,1)
	 and thus it's equation is (x^2, y^2, z) with normal (2x, 2y,1)
	 *************************************************/


	 Vector3D bottom_normal, top_normal, middle_normal, cap_normal;
	 float lamda_final,lamda_a,lamda_b;




	 //convert ray to object coordinates
	 Ray3D ray_object = Ray3D(worldToModel * ray.origin, worldToModel* ray.dir);
	 Point3D point_of_intersection;
	 Vector3D intersection_condition;
	 bool did_intersect = false;

	 //find intersection with bottom
	bottom_normal = Vector3D(0, 0, -1);
	top_normal = Vector3D(0, 0, 1);
	lamda_a = float (-0.5-ray_object.origin[2])/ ray_object.dir[2];
	lamda_b = float (0.5-ray_object.origin[2])/ ray_object.dir[2];
	lamda_final = fmin(lamda_a,lamda_b);
	if(lamda_a<=lamda_b){
		lamda_final=lamda_a;
		cap_normal = bottom_normal;
	}else if(lamda_b<lamda_a){
		lamda_final=lamda_b;
		cap_normal  = top_normal;
	}

	if (lamda_final<0)
		return false;

	point_of_intersection = (ray_object.origin + lamda_final * ray_object.dir);


	if( (1>=point_of_intersection[0])&& (point_of_intersection[0]>=-1)
		&& (1>=point_of_intersection[1] )&& (point_of_intersection[1]>=-1)
		&& (pow(point_of_intersection[0],2)+ pow(point_of_intersection[1],2) <=1)
		&& (ray.intersection.none || (lamda_final < ray.intersection.t_value))){

		//point of intersection in the world coordinates
		ray.intersection.point = modelToWorld * point_of_intersection;
		cap_normal.normalize();
		ray.intersection.normal = worldToModel.transpose()  * cap_normal;
		ray.intersection.normal.normalize();
		ray.intersection.t_value = lamda_final;
		ray.intersection.none = false;
		did_intersect = true;
		//ray.col = Colour(0.54, 0.89, 0.63);


	}



	//find intersection with middle

	/******************************************
	suppose ray(lamda) = a+ lamda*dir;
			cyliner equation = x^2+y^2-1 = 0
	then
		intersection between ray and middle parto of cylinder is when (a + lamda* dir)^2 - 1 = 0
		and point_of_intersection[2]<1 && point_of_intersection[2]>-1
			(a_x^2 + a_y^2 - 1)
			+ (2*a_x*dir_x + 2*a_y*dir_y)*lamda
			+ (dir_x^2 + dir_y^2)* lamda^2
			=0

			A = (dir_x^2 + dir_y^2);
			B = (2*a_x*dir_x + 2*a_y*dir_y);
			C = (a_x^2 + a_y^2 - 1) ;
			so lamda = (- (B) + - sqrt((B)^2 - 4* (A) * (C))) / 2(A)

	********************************************/

		double a[2] = {ray_object.origin[0], ray_object.origin[1]};
		double dir[2] = {ray_object.dir[0], ray_object.dir[1]};


		double A = dir[0]*dir[0] + dir[1]* dir[1];
		double B = 2*(a[0]*dir[0] + a[1]*dir[1]);
		double C = a[0]*a[0]+ a[1]*a[1] -1;
		double D = pow(B,2) - 4*A*C;

		float lamda_1 = float(-B + sqrt(D))/float(2*A);
		float lamda_2 = float(-B - sqrt(D))/float(2*A);

		//if D < 0 there is no intersection points
		if (D < 0){
			return false;
		}
		else{
			//if both lamda_1 and lamda_2 < 0 the both not visible
			if (lamda_1 < 0 && lamda_2 <0){
				return false;
			//if lamda_1>0 and lamda_2 > 0 then point of intersection is ray(lamda_1)
			}else if (lamda_1 > 0 && lamda_2 <0){

				//Check if the ray has not intersected any objects before or the point
				// of intersection is less than the point that was recorded before.
				if (ray.intersection.none || (lamda_1 < ray.intersection.t_value)){

					//calculate point of intersection
					point_of_intersection = ray_object.origin + lamda_1 * ray_object.dir;



					//see if the intersection point is whitinb our cylinder range
					if (point_of_intersection[2]>-0.5 && point_of_intersection[2]<0.5){
						//calculate the intersection point
						ray.intersection.point = modelToWorld * point_of_intersection;
						Vector3D normal_vector = - Vector3D(point_of_intersection[0],
														point_of_intersection[1],
														0);

						//calculate the normal vector
						//normal of sphere x^2-y^2-z^2 -1 = 0 is <2x, 2y, 2z>
						normal_vector.normalize();
						ray.intersection.normal = worldToModel.transpose() * normal_vector;
						ray.intersection.normal.normalize();
						ray.intersection.none = false;
						ray.intersection.t_value = lamda_1;
						//ray.col = Colour(0.3, 0.3, 0.3);
						return true;

					}



				}


			//point of intersection is ray(lamda_2) since it's the closest
			} else if (lamda_1 > lamda_2 && lamda_2 >0){

				//ch
				if  (ray.intersection.none || (lamda_2 < ray.intersection.t_value)){

					//calculate point of intersection
					point_of_intersection = ray_object.origin + lamda_2 * ray_object.dir;
					//see if the point of intersection is whithn our cylinder range
					if (point_of_intersection[2]>-0.5 && point_of_intersection[2]<0.5){

						Vector3D normal_vector = - Vector3D(point_of_intersection[0],
														  point_of_intersection[1],
															0);

						ray.intersection.point = modelToWorld * point_of_intersection;

						//calculate the normal vector
						normal_vector.normalize();
						ray.intersection.normal = modelToWorld.transpose() * normal_vector;
						ray.intersection.normal.normalize();
						ray.intersection.none = false;
						ray.intersection.t_value = lamda_2;
						//ray.col = Colour(0.3, 0.3, 0.3);
						return true;
					}

				}
			} else{
					return false;
				}


		}



	return did_intersect;
}

ObjModel::ObjModel(const char * path){

	bool ret = loadOBJ(path, faces);

	// printf("|| DUMP || \n ");
	// for(unsigned int i=0; i<vertices.size(); i++){
	//
	// 	printf("> %lf %lf %lf\n", vertices[i][0], vertices[i][1], vertices[i][2]);
	// }

	// printf("%lf\n", vertices[0][0]);
}

bool ObjModel::intersect( Ray3D& ray, const Matrix4x4& worldToModel,
		const Matrix4x4& modelToWorld ) {

	// transform ray to object coordinates
	Point3D p1, p2, p3;
	Vector3D e1, e2;	// Edge1, Edge2
	Vector3D P, Q, T;
	Vector3D normal;
	float det, inv_det, u, v;
	double lamda = 999;

	Ray3D ray_object = Ray3D(worldToModel *ray.origin, worldToModel * ray.dir);

	bool did_intersect = false;

	int face_count = 0;

	for(int i=0; i< faces.size(); i ++){

		// Vector3D normal = Vector3D(normals[i][0], normals[i][1], normals[i][2]);	//normal vector to plane

		p1 = faces[i].vertices[0];	//point on the plane
		p2 = faces[i].vertices[1];
		p3 = faces[i].vertices[2];

		// DEBUG //
		// printf("face no: %i\n", face_count);
		// printf("p1: %lf %lf %lf \n", p1[0], p1[1], p1[2]);
		// printf("p2: %lf %lf %lf \n", p2[0], p2[1], p2[2]);
		// printf("p3: %lf %lf %lf \n\n", p3[0], p3[1], p3[2]);

		face_count++;

		e1 = p2 - p1;
		e2 = p3 - p1;


		Vector3D pvec = ray_object.dir.cross(e2);
		det = e1.dot(pvec);
		#ifdef TEST_CULL        /* define TEST_CUL if culling is desired */
		    if (det < EPSILON)
		        continue;
		#else        /* the non-culling branch */
		    if (det > -EPSILON && det < EPSILON)
		        continue;
		#endif

		inv_det = 1 / det;

		// printf("inv_det: %lf\n", inv_det);
		// prepare to compute u
		Vector3D tvec = ray_object.origin - p1;
		u = tvec.dot(pvec) * inv_det;

		// printf("u: %lf\n", u);
		if(u < 0 || u > 1) continue;


		// prepare to compute v
		Vector3D qvec = tvec.cross(e1);
		v = ray_object.dir.dot(qvec) * inv_det;
		if(v < 0 || v > 1 || u + v > 1) continue;

		// calculate lambda, ray intersects triangle
		if ( (0 < e2.dot(qvec) * inv_det) && (e2.dot(qvec) * inv_det < lamda) ){
			did_intersect = true;
			lamda = e2.dot(qvec) * inv_det;


			// normal = Vector3D(normals[i][0], normals[i][1], normals[i][2]);	//normal vector to plane

			if(faces[i].hasNormal){
				normal = faces[i].normal;
			} else {
				normal = e1.cross(e2);
			}


		}

	}

	if(did_intersect) {
		Point3D point_of_intersection = (ray_object.origin + lamda * ray_object.dir);

		// printf("lamda: %f\n", lamda);
		// printf("POI: %lf %lf %lf \n", point_of_intersection[0], point_of_intersection[1], point_of_intersection[2]);

		if((ray.intersection.none || (lamda < ray.intersection.t_value))){

			//point of intersection in the world coordinates
			ray.intersection.point = modelToWorld * point_of_intersection;
			normal.normalize();
			ray.intersection.normal = worldToModel.transpose() * normal;
			ray.intersection.normal.normalize();
			ray.intersection.t_value = lamda;
			ray.intersection.none = false;
			// printf("YES\n");
			return true;
		}
	}


	return false;
}

// Parses obj file at path, and stores them
bool ObjModel::loadOBJ( const char * path,
    		std::vector < Face > & out_faces)
{
	std::vector< unsigned int > vertexIndices, uvIndices, normalIndices;
	std::vector< Point3D > temp_vertices;
	std::vector< Vector3D > temp_uvs;
	std::vector< Vector3D > temp_normals;

	FILE * file = fopen(path, "r");
	if( file == NULL ){
	    printf("Impossible to open the file !\n");
	    exit(0);
	}


	while( 1 ){
	    char lineHeader[128];
	    // read the first word of the line
	    int res = fscanf(file, "%s", lineHeader);
	    if (res == EOF)
	        break; // EOF = End Of File. Quit the loop.

	    // else : parse lineHeader
		if ( strcmp( lineHeader, "v" ) == 0 ){
			Point3D vertex;
		    fscanf(file, "%lf %lf %lf\n", &vertex[0], &vertex[1], &vertex[2] );
			// printf("%lf  %lf  %lf\n", vertex[0], vertex[1], vertex[2]);

		    temp_vertices.push_back(vertex);
		} else if ( strcmp( lineHeader, "vt" ) == 0 ){
			Vector3D uv;
		    fscanf(file, "%lf %lf\n", &uv[0], &uv[1] );

			// printf("%lf  %lf\n", uv[0], uv[1]);
		    temp_uvs.push_back(uv);
	 	} else if ( strcmp( lineHeader, "vn" ) == 0 ){
			Vector3D normal;
		    fscanf(file, "%lf %lf %lf\n", &normal[0], &normal[1], &normal[2] );

			// printf("%lf  %lf  %lf\n", normal[0], normal[1], normal[2]);

		    temp_normals.push_back(normal);
		} else if ( strcmp( lineHeader, "f" ) == 0 ){
		    std::string vertex1, vertex2, vertex3;
		    unsigned int vertexIndex[3], uvIndex[3], normalIndex[3];

			char line[100];
			if(fgets(line, 100, file) != NULL){
				Face face;

				int matches = sscanf(line, "%d/%d/%d %d/%d/%d %d/%d/%d\n", &vertexIndex[0], &uvIndex[0], &normalIndex[0], &vertexIndex[1], &uvIndex[1], &normalIndex[1], &vertexIndex[2], &uvIndex[2], &normalIndex[2] );

			    if (matches != 9){
					int matches = sscanf(line, "%d %d %d \n", &vertexIndex[0], &vertexIndex[1], &vertexIndex[2]);

					if(matches != 3){
						printf("File can't be read by our simple parser");
						exit(0);
					} else if (matches == 3) {
						face.vertices.push_back(temp_vertices[vertexIndex[0] - 1]);
						face.vertices.push_back(temp_vertices[vertexIndex[1] - 1]);
						face.vertices.push_back(temp_vertices[vertexIndex[2] - 1]);

						face.hasNormal = false;

						out_faces.push_back(face);
					}
				} else {

					face.vertices.push_back(temp_vertices[vertexIndex[0] - 1]);
					face.vertices.push_back(temp_vertices[vertexIndex[1] - 1]);
					face.vertices.push_back(temp_vertices[vertexIndex[2] - 1]);

					face.uvs.push_back(temp_uvs[vertexIndex[0] - 1]);
					face.uvs.push_back(temp_uvs[vertexIndex[1] - 1]);
					face.uvs.push_back(temp_uvs[vertexIndex[2] - 1]);

					int x, y, z;

					if(matches == 9){
						x = ((temp_normals[normalIndex[0] - 1][0] + temp_normals[normalIndex[1] - 1][0] + temp_normals[normalIndex[2] - 1][0]) / 3);
						y = ((temp_normals[normalIndex[0] - 1][1] + temp_normals[normalIndex[1] - 1][1] + temp_normals[normalIndex[2] - 1][1]) / 3);
						z = ((temp_normals[normalIndex[0] - 1][2] + temp_normals[normalIndex[1] - 1][2] + temp_normals[normalIndex[2] - 1][2]) / 3);

						face.normal = Vector3D(x, y ,z);

						face.hasNormal = true;
					}


					out_faces.push_back(face);
				}
			}
		}
	}



	return true;
}
