/***********************************************************
     Starter code for Assignment 3

     This code was originally written by Jack Wang for
		    CSC418, SPRING 2005

		implements light_source.h

***********************************************************/

#include <cmath>
#include <algorithm>
#include "light_source.h"

void PointLight::shade( Ray3D& ray ) {
	// TODO: implement this function to fill in values for ray.col
	// using phong shading.  Make sure your vectors are normalized, and
	// clamp colour values to 1.0.
	//
	// It is assumed at this point that the intersection information in ray
	// is available.  So be sure that traverseScene() is called on the ray
	// before this function.
	Vector3D Normal = ray.intersection.normal;
	Point3D hit_point = ray.intersection.point;
	Vector3D S_k = _pos - hit_point;
	Vector3D C_k = - ray.dir;
	Vector3D R_k = (2 * S_k.dot(Normal)) * Normal - S_k;


	Normal.normalize();
	S_k.normalize();
	C_k.normalize();
	R_k.normalize();



	double max1 = std::max(0.0,S_k.dot(Normal));
	double max2 = std::max(0.0,R_k.dot(C_k));


	// if an intersection happened
	if (ray.intersection.none == false){
		ray.col = max1 * ray.intersection.mat->diffuse * _col_diffuse +
			ray.intersection.mat->ambient * _col_ambient
			+ pow(max2,ray.intersection.mat->specular_exp)* ray.intersection.mat->specular;
		// ray.col = ray.intersection.mat->ambient * _col_ambient;
		ray.col.clamp();
	}

}
